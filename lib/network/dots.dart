import 'package:dio/dio.dart';
import 'package:journey/network/apis.dart';
import 'package:journey/generated/json/base/json_convert_content.dart';
import 'package:journey/models/dot_entity.dart';

class Dots {
  static Future<DotEntity> getDotList() {
    return Apis.get(
      'v1/journey',
      params: null,
    ).then((value) {
      if (value.statusCode != 200) {
        return Future.value();
      }
      var dotEntity =
      JsonConvert.fromJsonAsT<DotEntity>(value.data);
      return Future.value(dotEntity);
    });
  }
}
