import 'package:flutter/material.dart';

class ExerciseTutorialPage extends StatefulWidget {
  @override
  _ExerciseTutorialPageState createState() => _ExerciseTutorialPageState();
}

class _ExerciseTutorialPageState extends State<ExerciseTutorialPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Notification Page",
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          highlightColor: Colors.white,
          splashColor: Colors.white,
          icon: Icon(
            Icons.close,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
