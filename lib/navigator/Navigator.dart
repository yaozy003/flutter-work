import 'package:flutter/material.dart';
import 'package:journey/Pages/ChallengesPage.dart';
import 'package:journey/Pages/JourneyPage.dart';
import 'package:journey/Pages/ProfilePage.dart';
import 'package:journey/Pages/RewardsPage.dart';
import 'package:journey/Pages/WorkOutPage.dart';
import 'package:journey/models/map_dots.dart';

const double bottomBarHeight = 60;
const double appBarPercentage = 0.04;
const double excludAappBarHeightPercentage = 0.96;
final double bottomTabButtonSize = 0.0645;

//动态组件
class NavigatorPage extends StatefulWidget {
  NavigatorPage();

  @override
  _NavigatorPageState createState() => _NavigatorPageState();
}

class _NavigatorPageState extends State<NavigatorPage>
    with TickerProviderStateMixin<NavigatorPage> {
  AnimationController controller;
  Animation<double> easeInAnimation;

  @override
  void initState() {
    super.initState();

    //PageView Controller
    _pageController.addListener(() {
      setState(() {
        _currentIndex = (_pageController.page).round();
      });
    });
    //Animation
    controller = AnimationController(
        vsync: this,
        duration: Duration(
          milliseconds: 200,
        ),
        value: 1.0);
    easeInAnimation = Tween(begin: 1.0, end: 0.95).animate(
      CurvedAnimation(
        parent: controller,
        curve: Curves.easeIn,
      ),
    );
    controller.reverse();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  PageController _pageController = PageController();
  int _currentIndex = 3;

  //Dots
  static List<Dot> _dots = [];
  static int originalDotsAmount = 0;
  static Dot currentLockDot;
  static bool journeySelected = false,
      rewardsSelected = false,
      challengesSelected = false,
      profileSelected = false,
      workOutSelected = true;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width; //screen width
    double height = MediaQuery.of(context).size.height; //screen height
    return Scaffold(
      body: _currentPage(),
      bottomNavigationBar: SizedBox(
          height: bottomBarHeight,
          width: width,
          child: ButtomBar(context, width)),
    );
  }

  Stack ButtomBar(BuildContext context, double width) {
    return Stack(clipBehavior: Clip.none, children: [
      Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 6.0, 0.0, 2.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BottomTab(
                'assets/images/BottomNavi/journey_active.png',
                width,
                'assets/images/BottomNavi/journey_inactive.png',
                "Journey",
                context,
                1,
                journeySelected),
            BottomTab(
                'assets/images/BottomNavi/rewards_active.png',
                width,
                'assets/images/BottomNavi/rewards_inactive.png',
                "Rewards",
                context,
                2,
                rewardsSelected),
            Spacer(),
            BottomTab(
                'assets/images/BottomNavi/challenges_active.png',
                width,
                'assets/images/BottomNavi/challenges_inactive.png',
                "Challenges",
                context,
                3,
                challengesSelected),
            BottomTab(
                'assets/images/BottomNavi/profile_active.png',
                width,
                'assets/images/BottomNavi/profile_inactive.png',
                "Profile",
                context,
                4,
                profileSelected),
          ],
        ),
      ),
      Positioned(
        bottom: 20,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: RawMaterialButton(
                fillColor: Colors.white,
                shape: CircleBorder(),
                padding: EdgeInsets.all(4.0),
                child: workOutSelected
                    ? Image.asset(
                        'assets/images/BottomNavi/workout_active@2x.png',
                        height: width * 0.1866,
                        width: width * 0.1866,
                      )
                    : Image.asset(
                        'assets/images/BottomNavi/workout_inactive@2x.png',
                        height: width * 0.1866,
                        width: width * 0.1866,
                      ),
                onPressed: () {
                  setState(() {
                    journeySelected = false;
                    rewardsSelected = false;
                    challengesSelected = false;
                    profileSelected = false;
                    workOutSelected = true;
                    _currentIndex = 0;
                  });
                  _pageController.jumpToPage(0);
                }),
          ),
        ),
      ),
    ]);
  }

  TextButton BottomTab(
      final String activeImage,
      final double width,
      final String notActiveImage,
      final String tabText,
      BuildContext context,
      final int pageIndex,
      final bool tabSelected) {
    return TextButton(
        //splashColor: Colors.transparent,
        //highlightColor: Colors.transparent,
        child: Column(
          children: [
            tabSelected
                ? Image.asset(
                    activeImage,
                    height: bottomTabButtonSize * width,
                    width: bottomTabButtonSize * width,
                  )
                : Image.asset(
                    notActiveImage,
                    height: bottomTabButtonSize * width,
                    width: bottomTabButtonSize * width,
                  ),
            Text(
              tabText,
              style: Theme.of(context).textTheme.headline3,
            )
          ],
        ),
        onPressed: () {
          setState(() {
            workOutSelected = (pageIndex == 0);
            journeySelected = (pageIndex == 1);
            rewardsSelected = (pageIndex == 2);
            challengesSelected = (pageIndex == 3);
            profileSelected = (pageIndex == 4);
            _currentIndex = pageIndex;
          });
          _pageController.jumpToPage(pageIndex);
        });
  }

  Widget _currentPage() {
    List _pages = [
      WorkoutPage(),
      JourneyPage(),
      RewardsPage(),
      ChallengesPage(),
      ProfilePage(),
    ];
    return PageView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: _pages.length,
        controller: _pageController, //controller
        itemBuilder: (context, index) => _pages[index] //construct an page instance
        );
  }
}
