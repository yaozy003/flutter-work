import 'package:journey/models/dot_entity.dart';

dotEntityFromJson(DotEntity data, Map<String, dynamic> json) {
	if (json['dots_amount'] != null) {
		data.dotsAmount = json['dots_amount'] is String
				? int.tryParse(json['dots_amount'])
				: json['dots_amount'].toInt();
	}
	if (json['dots'] != null) {
		data.dots = new List<DotDot>();
		(json['dots'] as List).forEach((v) {
			data.dots.add(new DotDot().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> dotEntityToJson(DotEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['dots_amount'] = entity.dotsAmount;
	if (entity.dots != null) {
		data['dots'] =  entity.dots.map((v) => v.toJson()).toList();
	}
	return data;
}

dotDotFromJson(DotDot data, Map<String, dynamic> json) {
	if (json['dot_type'] != null) {
		data.dotType = json['dot_type'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['reward'] != null) {
		data.reward = new List<DotDotsReward>();
		(json['reward'] as List).forEach((v) {
			data.reward.add(new DotDotsReward().fromJson(v));
		});
	}
	if (json['reps'] != null) {
		data.reps = json['reps'] is String
				? int.tryParse(json['reps'])
				: json['reps'].toInt();
	}
	if (json['future'] != null) {
		data.future = json['future'];
	}
	if (json['animation'] != null) {
		data.animation = json['animation'].toString();
	}
	if (json['body'] != null) {
		data.body = json['body'].toString();
	}
	if (json['goals_per_week'] != null) {
		data.goalsPerWeek = json['goals_per_week'] is String
				? int.tryParse(json['goals_per_week'])
				: json['goals_per_week'].toInt();
	}
	if (json['rewarded_days'] != null) {
		data.rewardedDays = json['rewarded_days'] is String
				? int.tryParse(json['rewarded_days'])
				: json['rewarded_days'].toInt();
	}
	return data;
}

Map<String, dynamic> dotDotToJson(DotDot entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['dot_type'] = entity.dotType;
	data['status'] = entity.status;
	if (entity.reward != null) {
		data['reward'] =  entity.reward.map((v) => v.toJson()).toList();
	}
	data['reps'] = entity.reps;
	data['future'] = entity.future;
	data['animation'] = entity.animation;
	data['body'] = entity.body;
	data['goals_per_week'] = entity.goalsPerWeek;
	data['rewarded_days'] = entity.rewardedDays;
	return data;
}

dotDotsRewardFromJson(DotDotsReward data, Map<String, dynamic> json) {
	if (json['reward_type'] != null) {
		data.rewardType = json['reward_type'].toString();
	}
	if (json['amount'] != null) {
		data.amount = json['amount'].toString();
	}
	return data;
}

Map<String, dynamic> dotDotsRewardToJson(DotDotsReward entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['reward_type'] = entity.rewardType;
	data['amount'] = entity.amount;
	return data;
}