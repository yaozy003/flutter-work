import 'package:journey/generated/json/base/json_convert_content.dart';
import 'package:journey/generated/json/base/json_field.dart';

class DotEntity with JsonConvert<DotEntity> {
	@JSONField(name: "dots_amount")
	int dotsAmount;
	List<DotDot> dots;
}

class DotDot with JsonConvert<DotDot> {
	@JSONField(name: "dot_type")
	String dotType;
	String status;
	List<DotDotsReward> reward;
	int reps;
	bool future;
	String animation;
	String body;
	@JSONField(name: "goals_per_week")
	int goalsPerWeek;
	@JSONField(name: "rewarded_days")
	int rewardedDays;
}

class DotDotsReward with JsonConvert<DotDotsReward> {
	@JSONField(name: "reward_type")
	String rewardType;
	String amount;
}
