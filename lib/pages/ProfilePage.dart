import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:share/share.dart';
import '../pages/Setting.dart';
import 'NotificationPage.dart';
//import 'package:local_auth/local_auth.dart';

final double appBarPercentage = 0.04;
final double rewardsRectPercentage = 0.049;
const double BannerADHeight = 52.0;
final TextStyle cardTitleStyle = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: Colors.black,
    fontFamily: "GoogleFonts.lato");

class ProfilePage extends StatefulWidget {
  ProfilePage();

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  //final LocalAuthentication _localAuthentication = LocalAuthentication();

  // Future<bool> checkID() async {
  //   bool isAvailable = false;
  //   bool isAuthenticated = false;
  //   try {
  //     isAvailable = await _localAuthentication.canCheckBiometrics;
  //     if (isAvailable) {
  //       isAuthenticated = await _localAuthentication.authenticateWithBiometrics(
  //         localizedReason:
  //             "Please authenticate to view your transaction overview",
  //         useErrorDialogs: true,
  //         stickyAuth: true,
  //       );
  //     } else {
  //       print("biometric not available");
  //     }
  //   } on PlatformException catch (e) {
  //     print(e);
  //   }
  //   if (!mounted) return isAvailable;
  //   return isAuthenticated;
  // }

  // Future<List<BiometricType>> _availableBiometrics() async {
  //   List<BiometricType> listOfBiometrics;
  //   try {
  //     listOfBiometrics = await _localAuthentication.getAvailableBiometrics();
  //   } on PlatformException catch (e) {
  //     print(e);
  //   }
  //
  //   if (!mounted) return null;
  //
  //   print(listOfBiometrics);
  //   return listOfBiometrics;
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width; //screen width
    final double height = MediaQuery.of(context).size.height; //screen height

    //if (checkID() != false) {
    if (true) {
      return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(height * appBarPercentage),
          child: AppBar(
            leading: IconButton(
              splashColor: Colors.white,
              highlightColor:Colors.white,
              icon: Stack(
                children: <Widget>[
                  GestureDetector(
                    child: ImageIcon(
                      AssetImage(
                          "assets/images/ProfilePageImages/notification.png"),
                      color: Color(0xFF0389FF),
                    ),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => NotificationPage(),
                        ),
                      );
                    },
                  ),
                  Positioned(
                    right: 0,
                    top: 0,
                    child: Container(
                      padding: EdgeInsets.all(1),
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 12,
                        minHeight: 12,
                      ),
                      child: Text(
                        '2',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
              color: Colors.white,
              onPressed: () {},
            ),
            centerTitle: true,
            actions: [
              IconButton(
          splashColor: Colors.white,
                highlightColor:Colors.white,
                icon: Stack(
                  children: <Widget>[
                    ImageIcon(
                      AssetImage(
                          "assets/images/ProfilePageImages/settings.png"),
                      color: Color(0xFF0389FF),
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      child: Container(
                        padding: EdgeInsets.all(1),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        constraints: BoxConstraints(
                          minWidth: 12,
                          minHeight: 12,
                        ),
                        child: Text(
                          '2',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 8,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
                color: Colors.white,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return Setting();
                  }));
                },
              ),
            ],
            backgroundColor: Colors.white,
            elevation: 0,
            //remove shadow.
            title: Text(
              "PROFILE",
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
        ),
        body: ListView(
          children: [
            Container(height: BannerADHeight,width: width,color: Colors.red,child: Center(child: Text("BannerAD"),),),
            RewardsRect(width: width, height: height),
            AvatarRect(width: width, height: height),
            CurrentProgressRect(width: width, height: height),
            RepsAndWorkoutsCard(width: width, height: height),
            InviteRect(height: height, width: width)
          ],
        ),
      );
    } else {
      return Center(
        child: Text("User is not authenticated ",
            style: TextStyle(color: Colors.black)),
      );
    }
  }
}



class InviteRect extends StatelessWidget {
  const InviteRect({
    Key key,
    @required this.height,
    @required this.width,
  }) : super(key: key);

  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 21, 16, 50),
      child: Stack(
        overflow: Overflow.visible,
        children: [
          LimitedBox(
            maxHeight: height * 0.1879,
            child: Container(
              width: width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                gradient: LinearGradient(
                  colors: [Color(0xFF00C4FF), Color(0xFF0089FF)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(width * 0.04),
                child: Stack(
                  fit: StackFit.passthrough,
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      child: SizedBox(
                        width: width * 0.5706,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'INVITE A FRIEND',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: 'You and a friend will receive'),
                                  TextSpan(
                                    text: '\$5 each',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  TextSpan(
                                      text:
                                          ' when you both successfully subscribe to a paid subscription. Successful referrals during your trial period will be pending until you subscribe.'),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      child: SizedBox(
                        width: width * 0.1961,
                        height: height * 0.112,
                        child: Image.asset(
                            "assets/images/ProfilePageImages/hailBot.png"),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          //153x40 r29 b20
          Positioned(
            right: width * 0.08,
            bottom: -height * 0.0246,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(23),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      //blurRadius: 7
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ]),
              width: width * 0.408,
              height: height * 0.0492,
              child: GestureDetector(
                onTap: () {
                  Share.share('Fitbotic-AAbbop');
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Fitbotic-AABBCC",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue),
                    ),
                    Image.asset("assets/images/ProfilePageImages/share.png"),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class RepsAndWorkoutsCard extends StatelessWidget {
  const RepsAndWorkoutsCard({
    Key key,
    @required this.width,
    @required this.height,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          CardItem(
              width: width,
              height: height,
              cardTitle: "Total Reps",
              imageName: "assets/images/ProfilePageImages/jump-rope.png",
              numText: "1396"),
          CardItem(
              width: width,
              height: height,
              cardTitle: "Total Workouts",
              imageName: "assets/images/ProfilePageImages/skipping-rope.png",
              numText: "35"),
        ],
      ),
    );
  }
}

class CardItem extends StatelessWidget {
  const CardItem({
    Key key,
    @required this.width,
    @required this.height,
    @required this.cardTitle,
    @required this.imageName,
    @required this.numText,
  }) : super(key: key);

  final double width;
  final double height;
  final String cardTitle;
  final String imageName;
  final String numText;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 7.5),
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(15.0),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(
              width * 0.04, height * 0.016, width * 0.04, height * 0.007),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(cardTitle, style: cardTitleStyle),
                  Image.asset(
                    imageName,
                    height: 19.43,
                    width: 18.34,
                  )
                ],
              ),
              SizedBox(height: 6),
              Text(numText,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                      color: Color(0xFF0089FF))),
            ],
          ),
        ),
      ),
    );
  }
}

class CurrentProgressRect extends StatelessWidget {
  const CurrentProgressRect({
    Key key,
    @required this.width,
    @required this.height,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.fromLTRB(width * 0.04, 0, width * 0.04, height * 0.0184),
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        elevation: 1,
        child: Padding(
          padding: EdgeInsets.all(width * 0.048),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Current Progress",
                    style: cardTitleStyle,
                  ),
                  Image.asset(
                      "assets/images/ProfilePageImages/edit-curves .png")
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                      0,
                      height * 0.022,
                      0,
                      height * 0.007,
                    ),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          RotatedBox(
                            quarterTurns: 2,
                            child: CircularPercentIndicator(
                              radius: width * 0.224,
                              lineWidth: 7.0,
                              animation: true,
                              animationDuration: 2000,
                              percent: 1 / 3,
                              center: RotatedBox(
                                quarterTurns: 2,
                                child: Text(
                                  "1/3",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0),
                                ),
                              ),
                              circularStrokeCap: CircularStrokeCap.round,
                              progressColor: Colors.green,
                              //Color(0xFFEBEBEB)
                              backgroundColor: Color(0xFFEBEBEB),
                            ),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text("Daily Goal",
                              style: TextStyle(
                                  color: Color(0xFF777777),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14.0)),
                        ]),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                      0,
                      height * 0.022,
                      0,
                      height * 0.007,
                    ),
                    child: Column(children: [
                      RotatedBox(
                        quarterTurns: 2,
                        child: CircularPercentIndicator(
                            radius: width * 0.224,
                            lineWidth: 7.0,
                            animation: true,
                            animationDuration: 2000,
                            percent: 1 / 4,
                            center: RotatedBox(
                              quarterTurns: 2,
                              child: Text(
                                "1/4",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0),
                              ),
                            ),
                            circularStrokeCap: CircularStrokeCap.round,
                            progressColor: Colors.yellow,
                            backgroundColor: Color(0xFFEBEBEB)),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text("Weekly Goal",
                          style: TextStyle(
                              color: Color(0xFF777777),
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0)),
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                      0,
                      height * 0.022,
                      0,
                      height * 0.007,
                    ),
                    child: Column(
                      children: [
                        RotatedBox(
                          quarterTurns: 2,
                          child: CircularPercentIndicator(
                              radius: width * 0.224,
                              lineWidth: 7.0,
                              animation: true,
                              animationDuration: 2000,
                              percent: 1 / 3,
                              center: RotatedBox(
                                quarterTurns: 2,
                                child: Text(
                                  "1/3",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0),
                                ),
                              ),
                              circularStrokeCap: CircularStrokeCap.round,
                              progressColor: Colors.blue,
                              backgroundColor: Color(0xFFEBEBEB)),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text("Current Tier",
                            style: TextStyle(
                                color: Color(0xFF777777),
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0)),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AvatarRect extends StatelessWidget {
  const AvatarRect({
    Key key,
    @required this.width,
    @required this.height,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(width * 0.04,height * 0.032,width * 0.032,height * 0.0307 ),
            child: CircleAvatar(
              radius: width * 0.12,
              backgroundImage: NetworkImage(
                  "https://fitbotic.pixelforcesystems.com.au/packs/media/images/default-user-ab9e095c.png"),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Hello,{\$name}",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Colors.black),
              ),
              Text(
                "Look like you have been training.\n Keep up the good work!",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Colors.black),
              ),
              RawMaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  fillColor: Color(0xFFE5A539),
                  constraints: BoxConstraints(
                      minWidth: width * 0.362, minHeight: height * 0.0344),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Member: GOLD",
                        style: TextStyle(fontSize: 14),
                      ),
                      Image.asset(
                        'assets/images/ProfilePageImages/arrow-left.png',
                        width: height * 0.0147,
                        height: height * 0.0147,
                      ),
                    ],
                  ),
                  onPressed: null)
            ],
          )
        ],
      ),
    );
  }
}

class RewardsRect extends StatelessWidget {
  const RewardsRect({
    Key key,
    @required this.width,
    @required this.height,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(
        width * 0.0533,
        height * 0.0147,
        width * 0.0367,
        height * 0.0146,
      ),
      color: Color(0xFF0089FF),
      height: height * rewardsRectPercentage,
      width: width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "UNLOCK 10X MORE REWARDS",
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
          ),
          Image.asset(
            'assets/images/ProfilePageImages/arrow-left.png',
            width: height * 0.0147,
            height: height * 0.0147,
          ),
        ],
      ),
    );
  }
}
