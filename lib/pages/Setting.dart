import 'dart:io' show File, Platform;

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//packages
//import 'package:camera/camera.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

//other pages
import 'package:journey/JourneyTutorial.dart';

import '../ExerciseTutorial.dart';
import 'TrainerSettings.dart';

final double appBarPercentage = 0.04;

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  String txt = "See how it works";
  List<SelectableRRect> genderList = [
    SelectableRRect(
      txt: "Male",
      active: false,
    ),
    SelectableRRect(
      txt: "Female",
      active: false,
    ),
    SelectableRRect(
      txt: "Other",
      active: false,
    ),
    SelectableRRect(
      txt: "Prefer not to say",
      active: false,
    )
  ];
  List<SelectableRRect> frequenceList = [
    SelectableRRect(
      txt: "Never",
      active: false,
    ),
    SelectableRRect(
      txt: "1-2 Times",
      active: false,
    ),
    SelectableRRect(
      txt: "3-5 Times",
      active: false,
    ),
    SelectableRRect(
      txt: "5+ Times",
      active: false,
    )
  ];

  File image;

  final _picker = ImagePicker();
  DateTime selectedDate = DateTime.now();
  TextEditingController dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width; //screen width
    final double height = MediaQuery.of(context).size.height; //screen height

    void _seeHowitWorks(BuildContext context) {
      showCupertinoModalPopup<void>(
        context: context,
        builder: (BuildContext context) {
          return CupertinoActionSheet(
            actions: <Widget>[
              CupertinoActionSheetAction(
                  child: Text('Journey Tutorial'),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => JourneyTutorialPage(),
                      ),
                    );
                  }),
              CupertinoActionSheetAction(
                child: Text('Exercise Tutorial'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ExerciseTutorialPage(),
                    ),
                  );
                  Navigator.pop(context);
                },
              ),
            ],
            cancelButton: CupertinoActionSheetAction(
              isDefaultAction: true,
              child: Text('Cancel'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          );
        },
      );
    }

    void _contactSupport(BuildContext context) {
      print("contact support");
    }

    void _logOut(BuildContext context) {
      print("logout");
    }

    void _cancelSubscription(BuildContext context) {
      //TODO:cancel subsciption.
      print("cancel subscription");
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(height * appBarPercentage),
        child: AppBar(
          centerTitle: true,
          leading: RawMaterialButton(
            child: Icon(
              Icons.close,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            TextButton(
              onPressed: saveSettings(),
              child: Text(
                "Save",
                style: TextStyle(color: Color(0xFF0089FF)),
              ),
            ),
          ],
          backgroundColor: Colors.white,
          elevation: 0,
          //remove shadow.
          title: Text(
            "SETTINGS",
            style: TextStyle(
                fontSize: 14, fontWeight: FontWeight.w700, color: Colors.black),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: [
          //premium? null :
          UpgradeToPremiumRect(width: width, height: height),
          QuestionAndInput(
            width: width,
            height: height,
            question: "Hi, I’m Fitbotic. What’s your name?",
            mandantory: true,
            placeholder: " First Name ",
            errorMsg: "First name required",
          ),
          QuestionAndInput(
            width: width,
            height: height,
            question: " What’s your family name?",
            mandantory: true,
            placeholder: " Last Name ",
            errorMsg: "Last name required",
          ),
          //upload profile image
          Padding(
            padding: EdgeInsets.fromLTRB(
                width * 0.04, height * 0.0184, width * 0.04, 0),
            child: GestureDetector(
              onTap: () {
                if (Platform.isIOS) {
                  showCupertinoModalPopup<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return CupertinoActionSheet(
                        actions: <Widget>[
                          CupertinoActionSheetAction(
                              child: Text('View Profile Picture'),
                              onPressed: () {
                                Navigator.pop(context);
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //     builder: (context) {
                                //       return ViewImagePage();
                                //     },
                                //   ),
                                // );
                                Navigator.of(context).push(
                                  PageRouteBuilder(
                                    opaque: false, // set to false
                                    pageBuilder: (context, __, ___) =>
                                        ViewImagePage(),
                                  ),
                                );
                              }),
                          CupertinoActionSheetAction(
                            child: Text('Take Photo'),
                            onPressed: () async {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) {
                              //       return TakePictureScreen();
                              //     },
                              //   ),
                              // );
                              final shootedFile = await _picker.getImage(
                                source: ImageSource.camera,
                              );
                              setState(() {
                                if (shootedFile != null) {
                                  image = File(shootedFile.path);
                                } else {
                                  print("no photo shot.");
                                }
                              });
                              Navigator.pop(context);
                            },
                          ),
                          CupertinoActionSheetAction(
                            child: Text('Choose Photo'),
                            onPressed: () async {
                              final pickedFile = await _picker.getImage(
                                  source: ImageSource.gallery,
                                  imageQuality: 50);
                              //TODO: preview selected image.
                              setState(() {
                                if (pickedFile != null) {
                                  image = File(pickedFile.path);
                                } else {
                                  print('No image selected.');
                                }
                              });
                              Navigator.pop(context);
                            },
                          ),
                          CupertinoActionSheetAction(
                            isDestructiveAction: true,
                            child: Text('Remove Current Photo'),
                            onPressed: () {
                              setState(() {
                                image = null;
                              });
                              Navigator.pop(context);
                            },
                          ),
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          isDefaultAction: true,
                          child: Text('Cancel'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      );
                    },
                  );
                } else if (Platform.isAndroid) {}
                //TODO: open action list.
              },
              child: DottedBorder(
                  color: const Color(0xFFE8E8E8),
                  radius: const Radius.circular(10),
                  borderType: BorderType.RRect,
                  dashPattern: const [12, 12],
                  child: Container(
                    height: height * 0.0911,
                    child: Row(
                      children: [
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: width * 0.05333),
                          child: CircleAvatar(
                            radius: width * 0.064,
                            backgroundImage: image == null
                                ? NetworkImage(
                                    "https://fitbotic.pixelforcesystems.com.au/packs/media/images/default-user-ab9e095c.png")
                                : FileImage(image),
                          ),
                        ),
                        Row(
                          children: [
                            Text(
                              "Upload Profile Page",
                              style: const TextStyle(
                                  fontSize: 14, color: Color(0xFF222222)),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 3.0, bottom: 12),
                              child: Container(
                                width: 5,
                                height: 5,
                                decoration: const BoxDecoration(
                                  color: Colors.blue,
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )),
            ),
          ),
          QuestionAndInput(
              width: width,
              height: height,
              question: "Stay in the loop with Fitbotic",
              mandantory: true,
              placeholder: "Email",
              errorMsg: "Email required"),
          //select DOB
          Column(
            children: [
              Question(
                  width: width,
                  height: height,
                  question: "What's your age?",
                  mandantory: true),
              GestureDetector(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: width * 0.04, vertical: 0),
                  child: Container(
                    height: height * 0.0554,
                    child: TextField(
                      controller: dateController,
                      enabled: false,
                      decoration: InputDecoration(
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        suffixIcon: Image.asset(
                          "assets/images/ProfilePageImages/calendar-date.png",
                          width: width * 0.04,
                          height: width * 0.04,
                        ),
                        hintText: "Date of Birth",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: const BorderSide(
                              color: Color(0xFFE8E8E8), width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(
                              color: const Color(0xFFE8E8E8), width: 1),
                        ),
                      ),
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ),
                onTap: () {
                  _selectDate(context);
                },
              ),
            ],
          ),
          SelectRow(width, height, "What is your gender?", genderList),
          SelectRow(width, height, "How often do you exercise per week?",
              frequenceList),
          GestureDetector(
              child: Padding(
                padding: EdgeInsets.fromLTRB(15, 17, 15, 30),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: const Color(0xFFE8E8E8),
                    ),
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 14),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Trainer Settings",
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                        Text(">",
                            style: TextStyle(color: Colors.black, fontSize: 15))
                      ],
                    ),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return TrainerSettings();
                }));
              }),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Tab(txt: "See How It Works", function: _seeHowitWorks),
              Tab(txt: "Contact Support", function: _contactSupport),
              Tab(txt: "Logout", function: _logOut),
              Tab(txt: "Cancel Subscription", function: _cancelSubscription),
              Padding(
                padding: const EdgeInsets.fromLTRB(24, 0, 0, 30),
                child: Text(
                  'App Version 1.2.1',
                  style: TextStyle(
                    fontSize: 12,
                    color: Color(0xFF777777),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Column SelectRow(double width, double height, String questionTxt, List list) {
    return Column(
      children: [
        Question(
            width: width,
            height: height,
            question: questionTxt,
            mandantory: true),
        SizedBox(
          height: height * 0.0554,
          child: ListView.builder(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: 4,
            itemBuilder: (context, index) {
              return Row(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 14),
                    decoration: BoxDecoration(
                        color: list[index].active
                            ? const Color(0xFF222222)
                            : const Color(0xFFFFFFFF),
                        borderRadius: BorderRadius.circular(23),
                        border: Border.all(
                          color: list[index].active
                              ? const Color(0xFF222222)
                              : const Color(0xFFE8E8E8),
                        )),
                    child: GestureDetector(
                      child: Text(
                        list[index].txt,
                        style: list[index].active
                            ? const TextStyle(fontSize: 14, color: Colors.white)
                            : const TextStyle(
                                fontSize: 14, color: Colors.black),
                      ),
                      onTap: () {
                        setState(() {
                          for (SelectableRRect sr in list) {
                            sr.active = false;
                          }
                          list[index].active = true;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    width: width * 0.0266,
                  )
                ],
              );
            },
          ),
        )
      ],
    );
  }

  saveSettings() {
    ////TODO : validate && save settings to API.
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dateController..text = DateFormat('dd MMM yyyy').format(selectedDate);
      });
  }
}

class Tab extends StatelessWidget {
  const Tab({
    Key key,
    @required this.txt,
    @required this.function,
  }) : super(key: key);

  final String txt;
  final Function function;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.fromLTRB(24, 0, 36, 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              txt,
              style: TextStyle(color: Colors.black, fontSize: 14),
            ),
            Text(">", style: TextStyle(color: Colors.black, fontSize: 15))
          ],
        ),
      ),
      onTap: () {
        function(context);
        // _seeHowitWorks(context);
      },
    );
  }
}

class SelectableRRect extends StatefulWidget {
  SelectableRRect({
    @required this.txt,
    @required this.active,
  });

  final String txt;
  bool active;

  @override
  _SelectableRRectState createState() => _SelectableRRectState();
}

class _SelectableRRectState extends State<SelectableRRect> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 14),
      decoration: BoxDecoration(
          color: widget.active ? Color(0xFF222222) : Color(0xFFFFFFFF),
          borderRadius: BorderRadius.circular(23),
          border: Border.all(
            color: widget.active ? Color(0xFF222222) : Color(0xFFE8E8E8),
          )),
      child: GestureDetector(
        onTap: () {
          setState(() {
            widget.active = !widget.active;
          });
        },
        child: Text(
          widget.txt,
          style: widget.active
              ? TextStyle(fontSize: 14, color: Colors.white)
              : TextStyle(fontSize: 14, color: Colors.black),
        ),
      ),
    );
  }
}

class QuestionAndInput extends StatelessWidget {
  const QuestionAndInput({
    Key key,
    @required this.width,
    @required this.height,
    @required this.question,
    @required this.mandantory,
    @required this.placeholder,
    @required this.errorMsg,
  }) : super(key: key);

  final double width;
  final double height;
  final String question;
  final bool mandantory;
  final String placeholder;
  final String errorMsg;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Question(
            width: width,
            height: height,
            question: question,
            mandantory: mandantory),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.04, vertical: 0),
          child: Container(
            height: height * 0.0554,
            child: TextFormField(
              decoration: InputDecoration(
                floatingLabelBehavior: FloatingLabelBehavior.never,
                //errorText: errorMsg,
                hintText: placeholder,
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide:
                      const BorderSide(color: Color(0xFFE8E8E8), width: 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide:
                      BorderSide(color: const Color(0xFFE8E8E8), width: 1),
                ),
              ),
              validator: (val) {
                val = val.trim();
                if (mandantory && val.length == 0) {
                  return errorMsg;
                } else {
                  return null;
                }
              },
              keyboardType: TextInputType.name,
              style: TextStyle(fontSize: 14),
            ),
          ),
        ),
      ],
    );
  }
}

class Question extends StatelessWidget {
  const Question({
    Key key,
    @required this.width,
    @required this.height,
    @required this.question,
    @required this.mandantory,
  }) : super(key: key);

  final double width;
  final double height;
  final String question;
  final bool mandantory;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
          width * 0.064, height * 0.0197, width * 0.04, height * 0.0123),
      child: Row(
        children: [
          Text(
            question,
            style: TextStyle(fontSize: 14, color: Color(0xFF222222)),
          ),
          mandantory
              ? Padding(
                  padding: const EdgeInsets.only(left: 3.0, bottom: 12),
                  child: Container(
                    width: 5,
                    height: 5,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      shape: BoxShape.circle,
                    ),
                  ),
                )
              : null,
        ],
      ),
    );
  }
}

class UpgradeToPremiumRect extends StatelessWidget {
  const UpgradeToPremiumRect({
    Key key,
    @required this.width,
    @required this.height,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: width,
        height: height * 0.128,
        margin: EdgeInsets.fromLTRB(
            width * 0.04, height * 0.027, width * 0.04, height * 0.027),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(
              colors: [Color(0xFF00C4FF), Color(0xFF0089FF)],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
            )),
        child: Padding(
          padding: EdgeInsets.fromLTRB(
              width * 0.08, height * 0.0147, width * 0.08, height * 0.0147),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                  color: Colors.white),
              children: <TextSpan>[
                TextSpan(
                    text: 'Upgrade to Premium \n',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                    children: [
                      WidgetSpan(
                          child: Container(
                        height: height * 0.00738,
                      ))
                    ]),
                TextSpan(
                  text: 'Begin your Fitbotic Journey today! \n',
                ),
                TextSpan(
                    text:
                        'Track your progress and earn rewards with your Smarter Fitness Friend.'),
              ],
            ),
          ),
        ),
      ),
      onTap: () {
        //TODO: jump to upgrade page.
      },
    );
  }
}

class ViewImagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Container(
          color: Colors.white.withOpacity(0.1),
          child: Center(
            child: Image.network(
                "https://fitbotic.pixelforcesystems.com.au/packs/media/images/default-user-ab9e095c.png"),
          ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

// class TakePictureScreen extends StatefulWidget {
//   @override
//   _TakePictureScreenState createState() => _TakePictureScreenState();
// }
//
// class _TakePictureScreenState extends State<TakePictureScreen> {
//   List cameras;
//   CameraDescription firstCamera;
//   int selectedCameraIdx;
//   CameraController controller;
//   String imagePath;
//
//   Future<void> _initCameraControllerFuture;
//
//   @override
//   void initState() {
//     super.initState();
//
//     loadingCamera();
//     // WidgetsFlutterBinding.ensureInitialized();
//     // availableCameras().then((availableCameras) {
//     //   cameras = availableCameras;
//     //   if (cameras.length > 0) {
//     //     setState(() {
//     //       selectedCameraIdx = 0;
//     //     });
//     //     controller = CameraController(
//     //         cameras[selectedCameraIdx], ResolutionPreset.medium);
//     //     _initCameraControllerFuture = controller.initialize();
//     //   } else {
//     //     print("No camera available");
//     //   }
//     // }).catchError((err) {
//     //   print('Error: $err.code\nError Message: $err.message');
//     // });
//   }
//
//   Future<void> loadingCamera() async {
//     WidgetsFlutterBinding.ensureInitialized();
//     final cameras = await availableCameras();
//     // Get a specific camera from the list of available cameras.
//     if (cameras.length > 0) {
//       setState(() {
//         selectedCameraIdx = 0;
//       });
//       firstCamera = cameras.first;
//       controller = CameraController(firstCamera, ResolutionPreset.medium);
//       _initCameraControllerFuture = controller.initialize();
//     } else {
//       print("no camera available");
//     }
//   }
//
//   @override
//   void dispose() {
//     // Dispose of the controller when the widget is disposed.
//     controller.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: FutureBuilder<void>(
//         future: _initCameraControllerFuture,
//         builder: (context, snapshot) {
//           if (snapshot.connectionState == ConnectionState.done) {
//             // If the Future is complete, display the preview.
//             print(_initCameraControllerFuture);
//             print(snapshot.connectionState);
//             return CameraPreview(controller);
//           } else {
//             // Otherwise, display a loading indicator.
//             return Center(child: CircularProgressIndicator());
//           }
//         },
//       ),
//       floatingActionButton: FloatingActionButton(
//         child: Icon(Icons.camera_alt),
//         onPressed: () async {
//           try {
//             await _initCameraControllerFuture;
//             // String path = join(
//             //   // Store the picture in the temp directory.
//             //   // Find the temp directory using the `path_provider` plugin.
//             //   (await getTemporaryDirectory()).path,
//             //   '${DateTime.now()}.png',
//             // );
//             await controller.takePicture().then((XFile file) {
//               if (mounted) {
//                 setState(() {
//                   imagePath = file.path;
//                 });
//               }
//             });
//             Navigator.push(
//               context,
//               MaterialPageRoute(
//                 builder: (context) =>
//                     DisplayPictureScreen(imagePath: imagePath),
//               ),
//             );
//           } catch (e) {
//             print(e);
//           }
//         },
//       ),
//     );
//   }
// }
//
// class DisplayPictureScreen extends StatelessWidget {
//   final String imagePath;
//
//   const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Display the Picture')),
//       // The image is stored as a file on the device. Use the `Image.file`
//       // constructor with the given path to display the image.
//       body: Image.file(File(imagePath)),
//     );
//   }
// }
