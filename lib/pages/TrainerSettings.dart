import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TrainerSettings extends StatefulWidget {
  @override
  _TrainerSettingsState createState() => _TrainerSettingsState();
}

class _TrainerSettingsState extends State<TrainerSettings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("TrainerSetting"),
        leading: IconButton(
          highlightColor: Colors.white,
          splashColor: Colors.white,
          icon: Icon(Icons.close,color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
