import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/models/dot_entity.dart';
import 'package:journey/network/dots.dart';
import 'package:journey/widgets/PathPainter.dart';
import 'package:toggle_switch/toggle_switch.dart';

final double appBarPercentage = 0.04;
const double unitHeight = 250;
const double unitWidth = 354;

class JourneyPage extends StatefulWidget {
  @override
  _JourneyPageState createState() => _JourneyPageState();
}

class _JourneyPageState extends State<JourneyPage>
    with SingleTickerProviderStateMixin {
  int _widgetIndex = 0;

  //Animation Controller & Tween for dots.
  AnimationController _animationController;
  Animation<double> _doubleAnim;
  Future<DotEntity> futureData;

  @override
  void initState() {
    super.initState();
    futureData = _getDots();
    _animationController = AnimationController(
        vsync: this,
        duration: Duration(
          milliseconds: 2000,
        ),
        value: 1.0);
    _doubleAnim = Tween(begin: 0.8, end: 1.20).animate(_animationController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          //动画在结束时停止的状态
          _animationController.reverse(); //颠倒
        } else if (status == AnimationStatus.dismissed) {
          //动画在开始时就停止的状态
          _animationController.forward(); //向前
        }
      })
      ..addListener(() {
        if (this.mounted) {
          //setState(() { });
        }
      });
    _animationController.forward();
  }

  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Future<DotEntity> _getDots() async {
    return Dots.getDotList().then((value) {
      return Future.value(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width; //screen width
    final double height = MediaQuery.of(context).size.height; //screen height
    return Container(
        color: Colors.white,
        child: SafeArea(
            child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(height * appBarPercentage),
            child: AppBar(
              centerTitle: true,
              backgroundColor: Colors.white,
              elevation: 0, //remove shadow.
              title: Text(
                "JOURNEY",
                style: Theme.of(context).textTheme.headline1,
              ),
            ),
          ),
          body: Container(
            color: Colors.white,
            child: Column(
              children: [
                //试用通知
                ToggleSwitcher(),
                _widgetIndex == 0
                    ? MapView(width, context, height)
                    : ActivityView(),
              ],
            ),
          ),
        )));
  }

  Padding ToggleSwitcher() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 15.0),
      child: ToggleSwitch(
        minWidth: 170.0,
        minHeight: 33.0,
        fontSize: 14.0,
        initialLabelIndex: _widgetIndex,
        cornerRadius: 24.0,
        activeBgColor: Colors.blue,
        activeFgColor: Colors.white,
        inactiveBgColor: Colors.white,
        inactiveFgColor: Colors.grey,
        labels: ['Journey Map', 'Activity'],
        onToggle: (index) {
          setState(() {
            _widgetIndex = index;
            print(_widgetIndex);
          });
        },
      ),
    );
  }

  Expanded ActivityView() {
    return Expanded(
      child: Container(
        color: Colors.blue,
        width: double.infinity,
        child: Text(
          "Helo",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Expanded MapView(
    double width,
    BuildContext context,
    double height,
  ) {
    return Expanded(
      child: ListView(children: [
        FutureBuilder(
          future: futureData,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.none ||
                snapshot.data == null) {
              return Container(
                width: double.infinity,
                height: height,
                color: Colors.blue,
                child: Text(
                  "conection state none",
                  style: TextStyle(color: Colors.black),
                ),
              );
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return Container(
                width: double.infinity,
                height: height,
                color: Colors.blue,
                child: Center(
                  child: Text(
                    'loading...',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              );
            } else if (snapshot.connectionState == ConnectionState.active) {
              return Container(
                width: double.infinity,
                height: height,
                color: Colors.blue,
              );
            } else {
              print("snapshot.length: " + snapshot.data.dots.length.toString());
              int currentDotIndex = 0;
              for (int j = 0; j < snapshot.data.dots.length; j++) {
                if (snapshot.data.dots[j].status == "current") {
                  currentDotIndex = j;
                }
              }
              return Container(
                color: Colors.blue,
                child: Column(
                  children: [
                    Container(
                      color: Colors.blue,
                      width: width,
                      height: 230,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            //notification rect
                            width: width,
                            height: 28,
                            color: Colors.blue,
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      16.0, 10, 16.0, 0),
                                  child: const Image(
                                    image: AssetImage(
                                      'assets/images/JourneyPageImages/Badge.png',
                                    ),
                                  ),
                                ),
                                Column(
                                  //put two text on the top
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                      '{2} MORE GOALS TO EARN YOUR \$10 BONUS!',
                                      textAlign: TextAlign.left,
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                    ),
                                    Text(
                                      'You have {3} days left of your free trial.',
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Color(0xFF0A87BB),
                            ),
                            //Today's Goal

                            height: height * 0.18,
                            //Goal's box height
                            margin: EdgeInsets.all(15),
                            width: width,
                            child: Column(
                              //crossAxisAlignment: CrossAxisAlignment.stretch,
                              //crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Expanded(
                                    flex: 3,
                                    child: Center(
                                      child: Text(
                                        "TODAY'S GOAL",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                        textAlign: TextAlign.center,
                                      ),
                                    )),
                                Expanded(
                                  flex: 6,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 10,
                                        top: 0,
                                        right: 10,
                                        bottom: 10),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(15),
                                                bottomLeft: Radius.circular(15),
                                              ),
                                              color: Color(0xFF53ABCF),
                                            ),
                                            child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Text(
                                                    "COMPLETED",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText2,
                                                  ),
                                                  Text(
                                                    "0" +
                                                        " of " +
                                                        snapshot
                                                            .data
                                                            .dots[
                                                                currentDotIndex]
                                                            .reps
                                                            .toString(),
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline6,
                                                  ),
                                                  Text(
                                                    'REPS',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline6,
                                                  ),
                                                ]),
                                          ),
                                        ),
                                        SizedBox(width: 2),
                                        Expanded(
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(15),
                                                bottomRight:
                                                    Radius.circular(15),
                                              ),
                                              color: Color(0xFF53ABCF),
                                            ),
                                            child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    "EARN",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText2,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Image.asset(
                                                        'assets/images/JourneyPageImages/bot.png',
                                                        height: 27,
                                                        width: 27,
                                                      ),
                                                      SizedBox(width: 10),
                                                      Text(
                                                        (double.parse(snapshot
                                                                    .data
                                                                    .dots[
                                                                        currentDotIndex]
                                                                    .reward[0]
                                                                    .amount) *
                                                                1000)
                                                            .round()
                                                            .toString(),
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline6,
                                                      )
                                                    ],
                                                  ),
                                                  Text(
                                                    'COINS',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline6,
                                                  )
                                                ]),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          //TODO:Hide the notification when trial expires.
                        ],
                      ),
                    ),
                    Stack(children: [
                      SizedBox(
                        width: width,
                        height: height,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(
                            30,
                            0,
                            30,
                            0,
                          ),
                          child: CustomPaint(
                            painter: PathPainter(snapshot.data.dots),
                          ),
                        ),
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.dots.length ~/ 12 + 1,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.fromLTRB(
                              30,
                              0,
                              30,
                              0,
                            ),
                            child: SizedBox(
                              width: unitWidth,
                              height: unitHeight,
                              child: Stack(
                                overflow: Overflow.visible,
                                children: Draw(index, snapshot),
                              ),
                            ),
                          );
                        },
                      ),
                    ]),
                  ],
                ),
              );
            }
          },
        ),
      ]),
    );
  }

  List<Widget> Draw(int index, AsyncSnapshot<dynamic> snapshot) {
    final int originalDotsAmount = snapshot.data.dotsAmount;
    final List<DotDot> _dots = snapshot.data.dots;
    final List<List> twoDList = List.generate(12, (_) => new List(2));
    twoDList[0] = [-0.05, 0.0];
    twoDList[1] = [0.4, 2.15];
    twoDList[2] = [1.3, 2.85];
    twoDList[3] = [2.2, 3.05];
    twoDList[4] = [3.3, 3.2];
    twoDList[5] = [4.2, 3.4];
    twoDList[6] = [5.1, 4.9];
    twoDList[7] = [4.3, 6.6];
    twoDList[8] = [3.3, 6.7];
    twoDList[9] = [2.2, 6.85];
    twoDList[10] = [1.2, 7.15];
    twoDList[11] = [0.3, 8.0];
    index = 12 * index;

    List<Widget> widgetList = [];
    bool alignLeft = true;

    for (int j = 0; j < 12; j++) {
      if (index < originalDotsAmount) {
        int topTwo = 0;
        DotDot dot = _dots[index];
        if (topTwo < 2 && (dot.dotType == "weekly" || dot.dotType == "tier")) {
          widgetList.add(buildText(alignLeft, dot.body));
          alignLeft = !alignLeft;
          topTwo++;
        }
        index++;
        widgetList.add(buildDots(twoDList[j][0], twoDList[j][1], dot));
      }
    }

    return widgetList;
  }

  Widget buildDots(
    double left,
    double right,
    DotDot dot,
  ) {
    double dotSize = 0.0;
    String img = '';
    final double wunit = MediaQuery.of(context).size.width / 6;
    final double hunit = 300 / 12;
    if (dot.dotType == "daily") {
      dotSize = 30;
      switch (dot.status) {
        case "lock":
          {
            img = 'lock.png';
          }
          break;
        case "tick":
          {
            img = 'tick.png';
          }
          break;
        case "current":
          {
            img = 'doingDot.png';
            dotSize = _doubleAnim.value * dotSize;
          }
          break;
        // case Status.UNDONE:
        //   {
        //     img = 'cross.png';
        //   }
        //   break;
      }
    } else if (dot.dotType == "weekly") {
      dotSize = 43;

      switch (dot.status) {
        case "tick":
          {
            img = 'activeStar.png';
          }
          break;
        case "current":
          {
            img = 'doingDot.png';
            dotSize = _doubleAnim.value * dotSize;
          }
          break;
        case "lock":
          {
            img = 'inactiveStar.png';
          }
          break;
        // case Status.UNDONE:
        //   {
        //     img = 'cross.png';
        //   }
        //   break;
      }
    } else if (dot.dotType == "tier") {
      dotSize = 50;
      switch (dot.status) {
        case "tick":
          {
            img = 'tier_active.png';
          }
          break;
        case "current":
          {
            img = 'doingDot.png';
            dotSize = _doubleAnim.value * dotSize;
          }
          break;
        case "lock":
          {
            img = 'tier_inactive.png';
          }
          break;
        // case Status.UNDONE:
        //   {
        //     img = 'cross.png';
        //   }
        //   break;
        default:
          {
            img = 'cross.png';
          }
      }
    } else if (dot.dotType == null) {
      dotSize = 0;
      img = "cross.png";
    }

    return Positioned(
      //TODO：减少重建
      left: left * wunit - dotSize / 2,
      top: right * hunit - dotSize / 2,
      child: GestureDetector(
        //TODO：减少重建
        child: Container(
          //TODO：减少重建
          decoration: const BoxDecoration(
            color: Color(0xFFF4F4F4),
            shape: BoxShape.circle,
          ),
          child: Image.asset(
            //TODO：减少重建
            'assets/images/JourneyPageImages/' + img,
            width: dotSize,
            height: dotSize,
          ),
        ),
        onTap: () {
          showDialog<void>(
            context: context,
            barrierDismissible: true, // user must tap button!
            builder: (BuildContext context) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0))),
                contentPadding: const EdgeInsets.all(16.0),
                title: Text(
                  "Today's goal",
                  style: Theme.of(context).textTheme.headline1,
                  textAlign: TextAlign.center,
                ), //Alertbox title
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xFF53ABCF),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15)),
                      ),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.1,
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: Center(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'COMPLETE',
                                  style: Theme.of(context).textTheme.headline2,
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  dot.reps.toString() + ' REPS',
                                  style: Theme.of(context).textTheme.headline2,
                                  textAlign: TextAlign.center,
                                )
                              ],
                            )),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xFF53ABCF),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(15),
                            bottomRight: Radius.circular(15)),
                      ),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.1,
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'EARN',
                                  style: Theme.of(context).textTheme.headline2,
                                  textAlign: TextAlign.center,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/JourneyPageImages/bot@2x.png',
                                      height: 20,
                                      width: 20,
                                    ),
                                    Text(
                                      (double.parse(dot.reward[0].amount) * 100)
                                              .round()
                                              .toString() +
                                          ' Coins',
                                      style:
                                          Theme.of(context).textTheme.headline2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    RawMaterialButton(
                        fillColor: Colors.blue,
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(24.0)),
                        constraints: BoxConstraints.expand(
                            width: double.infinity, height: 30),
                        onPressed: null,
                        child: Text(
                          "SELECT WORKOUT",
                          style: Theme.of(context).textTheme.headline1,
                        )),
                    RawMaterialButton(
                      fillColor: Colors.grey,
                      shape: ContinuousRectangleBorder(
                          borderRadius: BorderRadius.circular(24.0)),
                      constraints: BoxConstraints.expand(
                          width: double.infinity, height: 30),
                      onPressed: null,
                      child: Text(
                        "I'll DO IT LATER",
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }

  Widget buildText(
    bool alignLeft,
    String text,
  ) {
    return Positioned(
      //TODO：减少重建
      right: alignLeft ? null : 40,
      left: alignLeft ? 50 : null,
      top: alignLeft ? -15 : 110,
      child: Container(
        //TODO：减少重建
        width: 300,
        height: 30,
        alignment: alignLeft ? Alignment.centerLeft : Alignment.centerRight,
        decoration: const BoxDecoration(
          color: Colors.blue,
          shape: BoxShape.rectangle,
        ),
        child: Text(
          //TODO：减少重建
          text,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ),
    );
  }
}
