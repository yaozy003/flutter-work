import 'package:flutter/material.dart';

class JourneyTutorialPage extends StatefulWidget {
  @override
  _JourneyTutorialPageState createState() => _JourneyTutorialPageState();
}

class _JourneyTutorialPageState extends State<JourneyTutorialPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("TrainerSetting"),
        leading: IconButton(
          highlightColor: Colors.white,
          splashColor: Colors.white,
          icon: Icon(Icons.close,color: Colors.black,),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
